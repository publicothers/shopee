<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Shopee Register</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<nav>
	    <div class="nav-wrapper grey-space orange-custom-text">
	  		<a href="#" class="brand-logo center black-text">Pendaftaran</a>
	  		<div class="back"><i class="material-icons">arrow_back</i></div>
	    </div>
  	</nav>
	<div id="form-pendaftaran" class="collection">
	  	<div class="collection-item grey-space"><?= isset($_SESSION['username']) ? 'Selamat Datang '.$_SESSION['username'] : 'Bergabunglah dalam program Penjual Terpilih Shopee! Isi form dibawah ini &amp; lampirkan foto diri beserta KTP.' ?></div>
	  	<a href="logout.php">Logout</a>
	   	<div class="collection-item title">LANGKAH 1</div>
	   	<div class="collection-item">
	   		<div class="outer">
	   			<div class="left-ktp">Masukkan No KTP</div>
	   			<div class="right-ktp">
	   				<input type="text" placeholder="No KTP" class="browser-default ktp" id="ktp">
	   			</div>
	   		</div>
	   	</div>
	   	<div class="space"></div>
	   	<div class="collection-item title">Langkah 2</div>    	
	   	<div class="collection-item">
	   		Foto diri beserta KTP Anda. No KTP dan wajah Anda harus terlihat jelas dalam foto
			<div class="outer">
				<div class="upload">
					<form action="#">
						<div class="file-field input-field foto">
					    	<div class="img-button">
					    		<img src="assets/img/upload.png" alt="">
					    		<input type="file">
					    	</div>
					    </div>
				    	<div class="text">Tambahkan Foto Anda</div>
					</form>
				</div>	
				<div class="upload">
					<form action="#">
						<div class="file-field input-field foto">
					    	<div class="img-button">
					    		<img src="assets/img/upload.png" alt="">
					    		<input type="file">
					    	</div>
					    </div>
				    	<div class="text">Tambahkan Foto KTP Anda</div>
					</form>
				</div>	
			</div>
	   	</div>
	   	<div class="space"></div>
	   	<div class="collection-item">
	   		<p>
		      <input type="checkbox" class="filled-in checkbox-orange" id="filled-in-box" />
		      <label for="filled-in-box">Saya Setuju dengan <a href="#">Syarat dan Ketentuan</a> program Penjual Terpilih Shopee</label>
		    </p>
	   	</div>
	   	<div class="space"></div>
	   	<div class="collection-item center">
	   		<a class="waves-effect waves-orange-custom orange-custom btn" onclick="submit();">Kirimkan</a>
	   	</div>
	</div>
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="assets/js/materialize.min.js"></script>
	<script>  	
		function submit(){
            var total = $("#ktp").val().trim().length;
            if (total<= 16) {
                alert("sukses")
            } else {
                alert("No KTP maksimal 16 Karakter")
            }
	    }	
		$(".ktp").keyup(function(e){
			if (/\D/g.test(this.value)){
				this.value = this.value.replace(/\D/g, '');
			}
		});
	</script>
</body>
</html>