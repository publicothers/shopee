<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Register</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<nav>
	    <div class="nav-wrapper grey-space orange-custom-text">
	  		<div class="back"><i class="material-icons">arrow_back</i></div>
	    </div>
  	</nav>
	<div class="container">
		<form class="col s12" action="proses1.php" method="post" id="buttonSubmit">
     		<div class="row">
		        	<div class="input-field col s12 form-username">
		            	<input id="first_name" type="text" class="validate mustNotEmpty username-field" value="<?= $_GET['username'] ?? '' ?>" name="username">
		            	<label for="first_name">Username</label>
		            	<div class="error-message">Username harus mengandung huruf dan angka</div>
		            </div>
		            <div class="input-field col s12 form-password">
		            	<input id="password" type="password" class="validate mustNotEmpty password-field" name="password">
		            	<label for="password">Password</label>
		            	<div class="error-message">Password harus 8-16 karakter</div>
		            </div>
		           	<input name="btnSubmit" type="hidden" value="true">
		            <button type="submit" class="waves-effect waves-orange-custom orange-custom btn" name="" value="submit">Submit</button>
		  </div>
	  </form>
	</div>
  <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="assets/js/materialize.min.js"></script>
  <script>
  	function usernameVal(username){
		var regex=/^(?![0-9]*$)(?![a-zA]*$)[a-zA-Z0-9]+$/;
		return regex.test(username);
	}

	$('#buttonSubmit').on('submit', function(e){
			e.preventDefault();
		$(".mustNotEmpty").each(function(){
				if(!$(this).val()){
					$(this).closest(".input-field").addClass("error-form");
					} else{
						$(this).closest(".input-field").removeClass("error-form");
					}

				/*validasi username*/
				if($(this).closest(".input-field").is(".form-username")){					
					var usernameText = $(".username-field").val();
					if(usernameText && usernameVal(usernameText)){
						$(".form-username").removeClass("error-form");
					}
					else{
						$(".form-username").addClass("error-form");
					}
				}

				/*validasi password*/
				if($(this).closest(".input-field").is(".form-password")){
					var totalPassword = $("#password").val().trim().length;
					var password = $("#password").val();
					var konfirmasi = $("#confPassword").val();

					if(totalPassword >= 8 && totalPassword <= 16){
						$(".form-password").removeClass("error-form");
					} else{
						$(".form-password").addClass("error-form");
					}
				}

			});

		if($(".error-form").length){
				if($(".error-form:first").find("input")){
					$('html, body').animate({
						scrollDown: $(".error-form:first").offset().top - 0
					}, 500);
					$(".error-form:first").find("input").focus();
				}else{
					$('html, body').animate({
						scrollDown: $(".error-form:first").offset().top - 0
					}, 500);
				}
			} else{ 
			window.location.href = "pendaftaran.php"
		};
  </script>


</body>
</html>