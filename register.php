<?php
	session_start();

	$db = mysqli_connect("localhost","root","","registercoba");
	
	if (isset($_POST['registerBtn']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$confPassword = $_POST['confPassword'];

		if ($password == $confPassword){
			$password = md5($password);
			
			$sql = "INSERT INTO users(username, email, password) VALUES('$username', '$email', '$password')";
			if (mysqli_query($db, $sql)) {
				echo '{"status":"success"}';
			} else {
				echo '{"status":"gagal"}';
			}
		} else {
			echo '{"status":"gagal"}';
		}

		die();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Register</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<nav>
	    <div class="nav-wrapper grey-space orange-custom-text">
	  		<div class="back"><i class="material-icons">arrow_back</i></div>
	    </div>
  	</nav>
	<div class="container">
			<div class="row">
		    	<form class="col s12" method="post" action="" id="buttonSubmit">
		        	<div class="input-field col s12 form-email">
		            	<input type="text" class="validate mustNotEmpty email-field" name="email">
		            	<label for="email">Email</label>
		            	<div class="error-message">Email masih salah</div>
		            </div>
		            <div class="input-field col s12 form-username">
		            	<input type="text" class="validate mustNotEmpty username-field" name="username">
		            	<label for="username">Username</label>
		            	<div class="error-message">Username harus mengandung huruf dan angka</div>
		            </div>
		            <div class="input-field col s12 form-password">
		            	<input id="password" type="password" class="validate mustNotEmpty" name="password">
		             	<label for="password">Password</label>
		             	<div class="error-message">Password harus 8-16 karakter</div>
    		        </div>
    		        <div class="input-field col s12 form-konfirmasi-password">
		            	<input id="confPassword" type="password" class="validate mustNotEmpty" name="confPassword">
		             	<label for="confPassword">Konfirmasi Password</label>
		             	<div class="error-message">Konfirmasi password belum cocok</div>
		           	</div>
		           	<input name="registerBtn" type="hidden" value="true">
		            <button type="submit" class="waves-effect waves-orange-custom orange-custom btn" name="" value="submit">Submit</button>
		        </form>
		  </div>
	  
	</div>
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="assets/js/materialize.min.js"></script>
	<script>
		function namaEmailVal(namaEmail){
			var regex=/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			return regex.test(namaEmail);
		}

		function usernameVal(username){
			var regex=/^(?![0-9]*$)(?![a-zA]*$)[a-zA-Z0-9]+$/;
			return regex.test(username);
		}

		$('#buttonSubmit').on('submit', function(e){
			e.preventDefault();

			$(".mustNotEmpty").each(function(){
				if(!$(this).val()){
					$(this).closest(".input-field").addClass("error-form");
					} else{
						$(this).closest(".input-field").removeClass("error-form");
					}
				/*validasi email*/
				if($(this).closest(".input-field").is(".form-email")){					
					var namaEmaila 	= $(".email-field").val();
					if(namaEmaila && namaEmailVal(namaEmaila)){
						$(".form-email").removeClass("error-form");
					}
					else{
						$(".form-email").addClass("error-form");
					}
				}

				/*validasi username*/
				if($(this).closest(".input-field").is(".form-username")){					
					var usernameText = $(".username-field").val();
					if(usernameText && usernameVal(usernameText)){
						$(".form-username").removeClass("error-form");
					}
					else{
						$(".form-username").addClass("error-form");
					}
				}

				/*validasi password*/
				if($(this).closest(".input-field").is(".form-password")){
					var totalPassword = $("#password").val().trim().length;
					var password = $("#password").val();
					var konfirmasi = $("#confPassword").val();

					if(totalPassword >= 8 && totalPassword <= 16){
						$(".form-password").removeClass("error-form");
					} else{
						$(".form-password").addClass("error-form");
					}
				}

				if($(this).closest(".input-field").is(".form-konfirmasi-password")){
					var totalPassword = $("#password").val().trim().length;
					var password = $("#password").val();
					var konfirmasi = $("#confPassword").val();

					if (password == konfirmasi){
						$(".form-konfirmasi-password").removeClass("error-form")
					} else {
						$(".form-konfirmasi-password").addClass("error-form")
					}
				}

			});
			
			if($(".error-form").length){
				if($(".error-form:first").find("input")){
					$('html, body').animate({
						scrollDown: $(".error-form:first").offset().top - 0
					}, 500);
					$(".error-form:first").find("input").focus();
				}else{
					$('html, body').animate({
						scrollDown: $(".error-form:first").offset().top - 0
					}, 500);
				}
			}else{
				$.post( 
					"register.php", 
					$('#buttonSubmit').serialize() 
				).done(function(data) {
					if (JSON.parse(data).status === 'success') {
					    window.location.href = "index.php?username=" + $("input[name=username]").val();
						$('input').val('');
					} else {
					    alert( "gagal" );
					}
				}).fail(function(data) {
				    alert( "error" );
				});
			} 
		});
		// }
	</script>
</body>
</html>